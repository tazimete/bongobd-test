package com.bongobdtest.videoplayer.presenter.videoplayer;

import com.bongobdtest.videoplayer.model.Video;
import com.bongobdtest.videoplayer.view.videoplayer.VideoPlayerView;

public class VideoPlayerPresenterImp implements VideoPlayerPresenter {
    private VideoPlayerView videoPlayerView;

    public VideoPlayerPresenterImp(VideoPlayerView videoPlayerView) {
        this.videoPlayerView = videoPlayerView;
    }

    @Override
    public void handlePlayVideo() {
        videoPlayerView.playVideo(new Video(1, "http://techslides.com/demos/sample-videos/small.mp4"));
    }

    @Override
    public void handlePauseVideo() {
        videoPlayerView.pauseVideo();
    }

    @Override
    public void handleRewindVideo(int currentPosition) {
        int position = currentPosition - 5000;
        videoPlayerView.rewindVideo(position);
    }

    @Override
    public void handleForwardVideo(int currentPosition) {
        int position = currentPosition - 5000;
        videoPlayerView.forwardVideo(position);
    }
}
