package com.bongobdtest.videoplayer.presenter.videoplayer;

public interface VideoPlayerPresenter {
    void handlePlayVideo();
    void handlePauseVideo();
    void handleRewindVideo(int currentPosition);
    void handleForwardVideo(int currentPosition);
}
