package com.bongobdtest.videoplayer.view.videoplayer;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import com.bongobdtest.videoplayer.R;
import com.bongobdtest.videoplayer.model.Video;
import com.bongobdtest.videoplayer.presenter.videoplayer.VideoPlayerPresenterImp;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback, VideoPlayerView {
    private VideoPlayerPresenterImp videoPlayerPresenterImp;
    private SurfaceView videoSurface;
    private MediaPlayer player;
    private Button btnPlay, btnPause, btnRewind, btnForward;
    private SurfaceHolder videoHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find Subviews
        findSubviews();

        //initialize
        initialize();

        //add Action To Subviews
        addActionToSubviews();
    }


    //find sub views
    private void findSubviews(){
        btnPlay = (Button) findViewById(R.id.btnPlay);
        btnPause = (Button) findViewById(R.id.btnPause);
        btnRewind = (Button) findViewById(R.id.btnRewind);
        btnForward = (Button) findViewById(R.id.btnForward);

        videoSurface = (SurfaceView) findViewById(R.id.surfaceView);
    }

    //initialize
    private void initialize(){
        videoHolder = videoSurface.getHolder();
        videoHolder.addCallback(this);
        videoHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        videoPlayerPresenterImp = new VideoPlayerPresenterImp(this);
        player = new MediaPlayer();
    }

    //add action to subviews
    private void addActionToSubviews(){
        //when click on  btnPlay
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //handle play video
                videoPlayerPresenterImp.handlePlayVideo();

            }
        });

        //when click on  btnPause
        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //handle pause video
                videoPlayerPresenterImp.handlePauseVideo();
            }
        });


        //when click on  btnRewind
        btnRewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //handle rewind video
                videoPlayerPresenterImp.handleRewindVideo(player.getCurrentPosition());
            }
        });


        //when click on  btnForward
        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //handle forward video
                videoPlayerPresenterImp.handleForwardVideo(player.getCurrentPosition());
            }
        });
    }


    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void playVideo(Video video) {
        try {
            player.setDataSource(video.getUrl());
            player.setDisplay(videoHolder);
            player.prepare();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.start();
    }

    @Override
    public void pauseVideo() {
        player.pause();
    }

    @Override
    public void rewindVideo(int position) {
        if (player == null) {
            return;
        }

        //set rewind position
        player.seekTo(position);
    }

    @Override
    public void forwardVideo(int position) {
        if (player == null) {
            return;
        }

        //set forward position
        player.seekTo(position);
    }
}
