package com.bongobdtest.videoplayer.view.videoplayer;

import com.bongobdtest.videoplayer.model.Video;

public interface VideoPlayerView {
    void playVideo(Video video);
    void pauseVideo();
    void rewindVideo(int position);
    void forwardVideo(int position);
}
