import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AnagramTest {

    @Test
    public void testIsAnagram(){
        assertEquals(true, Anagram.isAnagram("table", "ablet"));
        assertEquals(true, Anagram.isAnagram("red", "der"));
        assertEquals(true, Anagram.isAnagram("English", "lisheng"));
        assertEquals(false, Anagram.isAnagram("Tazim", "Tamim"));
        assertEquals(false, Anagram.isAnagram("Rickshaw", "WashKCRA"));
    }

    @Test
    public void testCheckAnagram(){
        assertEquals(true, Anagram.checkAnagram("table", "ebalt"));
        assertEquals(false, Anagram.checkAnagram("green", "grean"));
        assertEquals(true, Anagram.checkAnagram("Cricket", "rickect"));
        assertEquals(true, Anagram.checkAnagram("Football", "llabFOOt"));
        assertEquals(false, Anagram.checkAnagram("Bangladesh", "Banglavasa"));
    }

}
