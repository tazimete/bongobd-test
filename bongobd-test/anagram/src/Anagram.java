import java.util.Arrays;

public class Anagram {
    public static final String CLASS_NAME = "Anagram";

    public static void main(String[] args) {
        //testing anagram by isAnagram method
        if(isAnagram("table", "ablet")){
            System.out.println(CLASS_NAME+" -> isAnagram(table, ablet) = true");
        }else{
            System.out.println(CLASS_NAME+" -> isAnagram(table, ablet) = false");
        }

        //testing anagram by isAnagram method
        if(isAnagram("red", "der")){
            System.out.println(CLASS_NAME+" -> isAnagram(red, der) = true");
        }else{
            System.out.println(CLASS_NAME+" -> isAnagram(red, der) = false");
        }

        //testing anagram by isAnagram method
        if(isAnagram("English", "lisheng")){
            System.out.println(CLASS_NAME+" -> isAnagram(English, lisheng) = true");
        }else{
            System.out.println(CLASS_NAME+" -> isAnagram(English, lisheng) = false");
        }

        //testing anagram by isAnagram method
        if(isAnagram("Tazim", "Tamim")){
            System.out.println(CLASS_NAME+" -> isAnagram(Tazim, Tamim) = true");
        }else{
            System.out.println(CLASS_NAME+" -> isAnagram(Tazim, Tamim) = false");
        }

        //testing anagram by isAnagram method
        if(isAnagram("Rickshaw", "WashKCRA")){
            System.out.println(CLASS_NAME+" -> isAnagram(Rickshaw, WashKCRA) = true");
        }else{
            System.out.println(CLASS_NAME+" -> isAnagram(Rickshaw, WashKCRA) = false");
        }


        //testing anagram by checkAnagram method
        if(checkAnagram("table", "ebalt")){
            System.out.println(CLASS_NAME+" -> checkAnagram(table, ebalt) = true");
        }else{
            System.out.println(CLASS_NAME+" -> checkAnagram(table, ebalt) = false");
        }

        //testing anagram by checkAnagram method
        if(checkAnagram("green", "grean")){
            System.out.println(CLASS_NAME+" -> checkAnagram(green, grean) = true");
        }else{
            System.out.println(CLASS_NAME+" -> checkAnagram(green, grean) = false");
        }

        //testing anagram by checkAnagram method
        if(checkAnagram("Cricket", "rickect")){
            System.out.println(CLASS_NAME+" -> checkAnagram(Cricket, rickect) = true");
        }else{
            System.out.println(CLASS_NAME+" -> checkAnagram(Cricket, rickect) = false");
        }

        //testing anagram by checkAnagram method
        if(checkAnagram("Football", "llabFOOt")){
            System.out.println(CLASS_NAME+" -> checkAnagram(Football, llabFOOt) = true");
        }else{
            System.out.println(CLASS_NAME+" -> checkAnagram(Football, llabFOOt) = false");
        }

        //testing anagram by isAnagram method
        if(checkAnagram("Bangladesh", "Banglavasa")){
            System.out.println(CLASS_NAME+" -> isAnagram(Bangladesh, Banglavasa) = true");
        }else{
            System.out.println(CLASS_NAME+" -> isAnagram(Bangladesh, Banglavasa) = false");
        }

    }


    //check values are anagram or not
    public static boolean isAnagram(String s1, String s2){
        //check if value is null
        if ( s1 == null  ||  s2 == null ) {
            return false;
        }

        //check value length
        if ( s1.length() != s2.length() ) {
            return false;
        }

        //making string value to lower case and char array
        char[] c1 = s1.toLowerCase().toCharArray();
        char[] c2 = s2.toLowerCase().toCharArray();

        //sorting array
        Arrays.sort(c1);
        Arrays.sort(c2);

        //making new string
        String sc1 = new String(c1);
        String sc2 = new String(c2);

        return sc1.equals(sc2);
    }

    //check if values are anagram or not
    public static boolean checkAnagram(String s1, String s2){
        //check id value is null
        if ( s1 == null  ||  s2 == null ) {
            return false;
        }

        //check if value length
        if (s1.length() != s2.length()) {
            return false;
        }

        char[] c1 = s1.toLowerCase().toCharArray();
        char[] c2 = s2.toLowerCase().toCharArray();

        Arrays.sort(c1);
        Arrays.sort(c2);

        for(int i = 0; i < c1.length; i++) {
            if(c1[i] != c2[i]) return false;
        }

        return true;
    }

}
