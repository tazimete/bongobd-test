package factory_abstract;

abstract class Vehicle {
    abstract int get_num_of_wheels();
    abstract int get_num_of_passengers();
    abstract boolean has_gas();
}
