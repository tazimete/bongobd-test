package factory_abstract;

public class Car extends Vehicle {
    //Properties
    private int numOfWheels;
    private int numOfPassengers;
    private boolean hasGas;


    public void set_num_of_wheels(int numOfWheels) {
        this.numOfWheels = numOfWheels;
    }

    public void set_num_of_passengers(int numOfPassengers) {
        this.numOfPassengers = numOfPassengers;
    }

    public void set_has_gas(boolean hasGas) {
        this.hasGas = hasGas;
    }

    @Override
    public int get_num_of_wheels() {
        return numOfWheels;
    }

    @Override
    public int get_num_of_passengers() {
        return numOfPassengers;
    }

    @Override
    public boolean has_gas() {
        return hasGas;
    }
}
