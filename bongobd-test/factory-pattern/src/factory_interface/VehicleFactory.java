package factory_interface;

public class VehicleFactory {
    public static final String CLASS_NAME = "VehicleFactory";

    public static void main(String[] args) {
        //car
        Car car = (Car) getVehicleByType("CAR");
        car.set_num_of_wheels(4);
        car.set_num_of_passengers(8);
        car.set_has_gas(true);

        System.out.println(CLASS_NAME+" -- main() -- car -- num_of_wheels = "+car.get_num_of_wheels());
        System.out.println(CLASS_NAME+" -- main() -- car -- num_of_passengers = "+car.get_num_of_passengers());
        System.out.println(CLASS_NAME+" -- main() -- car -- has_gas = "+car.has_gas());

        //plane
        Plane plane = (Plane) getVehicleByType("PLANE");
        plane.set_num_of_wheels(12);
        plane.set_num_of_passengers(800);
        plane.set_has_gas(true);

        System.out.println(CLASS_NAME+" -- main() -- plane -- num_of_wheels = "+plane.get_num_of_wheels());
        System.out.println(CLASS_NAME+" -- main() -- plane -- num_of_passengers = "+plane.get_num_of_passengers());
        System.out.println(CLASS_NAME+" -- main() -- plane -- has_gas = "+plane.has_gas());
    }

    public static Vehicle getVehicleByType(String vehicleType){
        //if vehicle type is null
        if(vehicleType == null){
            return null;
        }

        //if vehicle type is car
        if(vehicleType.equalsIgnoreCase("CAR")){
            return new Car();
        }else if(vehicleType.equalsIgnoreCase("PLANE")){
            return new Plane();
        }

        return null;
    }
}
