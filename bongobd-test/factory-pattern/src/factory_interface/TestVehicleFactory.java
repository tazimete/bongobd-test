package factory_interface;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestVehicleFactory {

    @Test
    public void CarTest(){
        Car car = (Car) VehicleFactory.getVehicleByType("CAR");
        car.set_num_of_wheels(4);
        car.set_num_of_passengers(8);
        car.set_has_gas(true);

        assertEquals(4, car.get_num_of_wheels());
        assertEquals(8,car.get_num_of_passengers());
        assertEquals(true, car.has_gas());
    }

    @Test
    public void PlaneTest(){
        Plane plane = (Plane) VehicleFactory.getVehicleByType("PLANE");
        plane.set_num_of_wheels(12);
        plane.set_num_of_passengers(800);
        plane.set_has_gas(true);

        assertEquals(12, plane.get_num_of_wheels());
        assertEquals(800, plane.get_num_of_passengers());
        assertEquals(true, plane.has_gas());
    }
}
