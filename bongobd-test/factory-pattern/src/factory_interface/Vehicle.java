package factory_interface;

interface Vehicle {
    int get_num_of_wheels();
    int get_num_of_passengers();
    boolean has_gas();
}
